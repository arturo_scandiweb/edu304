window.onload = window.onresize = setWidth;

function setWidth() {
    var width = Math.max(document.documentElement.clientWidth, window.innerWidth || 0);
    var mobileVer = document.getElementById("mobile_tables");
    var desktopVer = document.getElementById("desktop_table");

    if(width > 768) {
        mobileVer.style.display = "none";
        desktopVer.style.display = "table";
    }else {
        mobileVer.style.display = "table";
        desktopVer.style.display = "none";
    }
}
