<?php
/**
 * @category    Edu304
 * @package     Edu304/EnDeStoresMigration
 * @author      Arturo Arakelyan <arturo@scandiweb.com>
 * @copyright   Copyright (c) 2019 Scandiweb, Inc (https://scandiweb.com)
 * @license     http://opensource.org/licenses/OSL-3.0 The Open Software License 3.0 (OSL-3.0)
 */

use \Magento\Framework\Component\ComponentRegistrar;

ComponentRegistrar::register(
    ComponentRegistrar::MODULE,
    'Edu304_EnDeStoresMigration',
    __DIR__
);
