<?php
/**
 * @category    Edu304
 * @package     Edu304/EnDeStoresMigration
 * @author      Arturo Arakelyan <arturo@scandiweb.com>
 * @copyright   Copyright (c) 2019 Scandiweb, Inc (https://scandiweb.com)
 * @license     http://opensource.org/licenses/OSL-3.0 The Open Software License 3.0 (OSL-3.0)
 */

namespace Edu304\EnDeStoresMigration\Setup;

use Magento\Framework\Event\ManagerInterface;
use Magento\Framework\Setup\InstallDataInterface;
use Magento\Framework\Setup\ModuleContextInterface;
use Magento\Framework\Setup\ModuleDataSetupInterface;
use Magento\Store\Model\GroupFactory;
use Magento\Store\Model\ResourceModel\Group;
use Magento\Store\Model\ResourceModel\Store;
use Magento\Store\Model\ResourceModel\Website;
use Magento\Store\Model\StoreFactory;
use Magento\Store\Model\WebsiteFactory;
use Magento\Framework\App\Config\Storage\WriterInterface;
use Magento\Framework\Setup\ModuleContextInterface;
use Magento\Framework\Setup\ModuleDataSetupInterface;
use Magento\Framework\Setup\UpgradeDataInterface;
use Magento\Store\Model\ScopeInterface;
use Magento\Store\Model\StoreManagerInterface;
use Magento\Theme\Model\ResourceModel\Theme\CollectionFactory;

/**
 * Class InstallData
 * @package Edu304\EnDeStoresMigration\Setup
 */
class InstallData implements InstallDataInterface
{
    /*
     * @var groupFactory
     */
    private $groupFactory;

    /*
     * @var websiteFactory
     */
    private $websiteFactory;

    /*
     * @var storeFactory
     */
    private $storeFactory;

    /*
     * @var groupResourceModel
     */
    private $groupResourceModel;

    /*
     * @var storeResourceModel
     */
    private $storeResourceModel;

    /*
     * @var websiteResourceModel
     */
    private $websiteResourceModel;

    /*
     * @var eventManager
     */
    private $eventManager;

    /*
     * @var coreConfigWriter
     */
    private $coreConfigWriter;

    /*
     * @var storeManager
     */
    private $storeManager;

    /*
     * @var themeCollectionFactory
     */
    private $themeCollectionFactory;

    /*
     * EN Store Configuration
     */
    const EN_STORE_CONFIGS = [
        [
            'path' => 'currency/options/default',
            'value' => 'GBP'
        ],
        [
            'path' => 'currency/options/allow',
            'value' => 'GBP'
        ],
        [
            'path' => 'currency/country/default',
            'value' => 'GB'
        ],
        [
            'path' => 'currency/country/destination',
            'value' => 'GB'
        ],
        [
            'path' => 'currency/locale/code',
            'value' => 'en_GB'
        ]
    ];

    /*
     * DE Store Configuration
     */
    const DE_STORE_CONFIGS = [
        [
            'path' => 'currency/options/default',
            'value' => 'EUR'
        ],
        [
            'path' => 'currency/options/allow',
            'value' => 'EUR'
        ],
        [
            'path' => 'currency/country/default',
            'value' => 'DE'
        ],
        [
            'path' => 'currency/country/destination',
            'value' => 'DE'
        ],
        [
            'path' => 'currency/locale/code',
            'value' => 'de_DE'
        ]
    ];

    /**
     * InstallData constructor.
     * @param GroupFactory $groupFactory
     * @param WebsiteFactory $websiteFactory
     * @param StoreFactory $storeFactory
     * @param Group $groupResourceModel
     * @param Store $storeResourceModel
     * @param Website $websiteResourceModel
     * @param ManagerInterface $eventManager
     * @param WriterInterface $coreConfigWriter
     * @param StoreManagerInterface $storeManager
     * @param CollectionFactory $themeCollectionFactory
     */
    public function __construct(
        GroupFactory $groupFactory,
        WebsiteFactory $websiteFactory,
        StoreFactory $storeFactory,
        Group $groupResourceModel,
        Store $storeResourceModel,
        Website $websiteResourceModel,
        ManagerInterface $eventManager,
        WriterInterface $coreConfigWriter,
        StoreManagerInterface $storeManager,
        CollectionFactory $themeCollectionFactory
    ) {
        $this->groupFactory = $groupFactory;
        $this->websiteFactory = $websiteFactory;
        $this->storeFactory = $storeFactory;
        $this->groupResourceModel = $groupResourceModel;
        $this->storeResourceModel = $storeResourceModel;
        $this->websiteResourceModel = $websiteResourceModel;
        $this->eventManager = $eventManager;
        $this->coreConfigWriter = $coreConfigWriter;
        $this->storeManager = $storeManager;
        $this->themeCollectionFactory = $themeCollectionFactory;
    }

    /*
     * install
     *
     * @param ModuleDataSetupInterface $setup
     * @param ModuleContextInterface $context
     *
     * @return void
     */
    public function install(ModuleDataSetupInterface $setup, ModuleContextInterface $context)
    {
        $setup->startSetup();
        $storeAttributes = [
            [
                'code' => 'gb',
                'group' => 'Main Website Store',
                'group_code' => 'main_website_store',
                'store_name' => 'English',
                'is_active' => '1'
            ],
            [
                'code' => 'de',
                'group' => 'Main Website Store',
                'group_code' => 'main_website_store',
                'store_name' => 'German',
                'is_active' => '1'
            ]
        ];

        foreach ($storeAttributes as $attribute) {
            $store = $this->storeFactory->create();
            $store->load($attribute['code']);

            if (!$store->getId()) {
                $website = $this->getWebsite();
                $group = $this->getGroup($website, $attribute);

                $store->setCode($attribute['code']);
                $store->setName($attribute['store_name']);
                $store->setWebsite($website);
                $store->setGroupId($group->getId());
                $store->setData('is_active', $attribute['is_active']);
                $this->storeResourceModel->save($store);
                $this->eventManager->dispatch('store_add', ['store' => $store]);
            }
        }
        $setup->endSetup();
    }

    /*
     * Get Group
     *
     * @param string $website
     * @param string $attributes
     *
     * @return $group
     */
    public function getGroup($website, $attributes)
    {
        $group = $this->groupFactory->create();
        $group->load($attributes['group_code'], 'code');
        if (!$group->getCode()) {
            $group->setWebsiteId($website->getWebsiteId());
            $group->setName($attributes['group']);
            $this->groupResourceModel->save($group);
        }

        return $group;
    }

    /*
     * Get Website
     *
     * @return string $website
     */
    public function getWebsite()
    {
        $website = $this->websiteFactory->create();
        $website->load('base');
        if (!$website->getId()) {
            $website->setCode('base');
            $website->setName('Main Website');
            $this->websiteResourceModel->save($website);
        }

        return $website;
    }

    /*
     * Set Default Properties
     *
     * @return void
     */
    private function setDefaultProperties()
    {
        $this->coreConfigWriter->save('currency/options/base', 'EUR', 'default', 0);
        $this->coreConfigWriter->save('currency/options/default', 'EUR', 'default', 0);
        $this->coreConfigWriter->save('currency/options/allow', 'EUR,GBP', 'default', 0);
        $this->coreConfigWriter->save('catalog/seo/product_url_suffix', null, 'default', '0');
        $this->coreConfigWriter->save('catalog/seo/category_url_suffix', null, 'default', '0');
    }

    /*
     * Load Theme
     *
     * @param string $name
     *
     * @return void
     */
    private function loadTheme($name)
    {
        $return = '';
        $themes = $this->themeCollectionFactory->create()->loadRegisteredThemes();
        foreach ($themes as $theme) {
            if ($theme->getCode() == $name) {
                $return = $theme->getId();
            }
        }

        return $return;
    }

    /*
     * Store Saver
     *
     * @param string $storeType
     * @param string $scope
     * @param int $id
     * @param string $themeName
     *
     * @return void
     */
    private function storeSaver($storeType, $scope, $id, $themeName)
    {
        foreach ($storeType as $storeAtt) {
            $this->coreConfigWriter->save($storeAtt['path'], $storeAtt['value'], $scope, $id);
        }
        $this->coreConfigWriter->save('design/theme/theme_id', $this->loadTheme($themeName), $scope, $id);
    }

    /*
     * Set Store Properties
     *
     * @param string $enStore
     * @param string $deStore
     *
     * @return void
     */
    private function setStoreProperties($enStore, $deStore)
    {
        $stores = $this->storeManager->getStores(true, false);
        $scope = ScopeInterface::SCOPE_STORES;

        foreach ($stores as $store) {
            $id = $store->getId();
            if ($store->getCode() == 'de') {
                $themeName = 'Scandi/German';
                $this->storeSaver($deStore, $scope, $id, $themeName);
            } elseif ($store->getCode() == 'gb') {
                $themeName = 'Scandi/Default';
                $this->storeSaver($enStore, $scope, $id, $themeName);
            }
        }
    }
}