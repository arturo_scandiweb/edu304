<?php
/**
 * @category    Edu304
 * @package     Edu304/CMSPage
 * @author      Arturo Arakelyan <arturo@scandiweb.com>
 * @copyright   Copyright (c) 2019 Scandiweb, Inc (https://scandiweb.com)
 * @license     http://opensource.org/licenses/OSL-3.0 The Open Software License 3.0 (OSL-3.0)
 */

namespace EDU304\CMSPage\Setup;

use Magento\Framework\Setup\UpgradeDataInterface;
use Magento\Framework\Setup\ModuleDataSetupInterface;
use Magento\Framework\Setup\ModuleContextInterface;
use Magento\Cms\Model\PageFactory;
use Magento\Cms\Model\PageRepository;

class UpgradeData implements UpgradeDataInterface
{
    private $pageFactory;
    private $pageRepository;

    public function __construct(PageFactory $pageFact, PageRepository $pageRepo)
    {
        $this->pageFactory = $pageFact;
        $this->pageRepository = $pageRepo;
    }

    public function upgrade(ModuleDataSetupInterface $setup, ModuleContextInterface $context)
    {
        $content = file_get_contents('app/code/Edu304/CMSPage/Setup/index.html');
        if (version_compare($context->getVersion(), '1.0.0', '<')) {
            $data = [
                'title' => 'Task 2',
                'content' => $content,
                'identifier' => 'task2',
                'is_active' => 1
            ];
            $page = $this->pageFactory->create();
            $page->setData($data);
            $this->pageRepository->save($page);
        }
    }
}
