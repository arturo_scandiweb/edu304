const gulp = require('gulp');
const sass = require('gulp-sass');
const del = require('del');

gulp.task('styles', () => {
    return gulp.src('web/styles/scss/*.scss')
        .pipe(sass().on('error', sass.logError))
        .pipe(gulp.dest('web/styles/css/'));
});

gulp.task('clean', () => {
    return del([
        'css/styles.css',
    ]);
});

gulp.task('default', gulp.series(['clean', 'styles']));