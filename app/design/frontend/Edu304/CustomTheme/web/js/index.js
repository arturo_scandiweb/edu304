/**
 * @category    Edu304
 * @package     Edu304/CustomTheme
 * @author      Arturo Arakelyan <arturo@scandiweb.com>
 * @copyright   Copyright (c) 2018 Scandiweb, Inc (https://scandiweb.com)
 * @license     http://opensource.org/licenses/OSL-3.0 The Open Software License 3.0 (OSL-3.0)
 */
define([], function () {
    'use strict';
    
    return function () {
        window.onload = window.onresize = setWidth;

        function setWidth() {
            var width = Math.max(document.documentElement.clientWidth, window.innerWidth || 0);
            var mobileVer = document.getElementById('mobile-tables');
            var desktopVer = document.getElementById('desktop-table');
            var float_table = document.getElementsByClassName('wrappable');

            if (width > 750) {
                mobileVer.style.display = 'none';
                desktopVer.style.display = 'table';
                float_table[0].style.margin = '-130px 0 0 450px';
            } else {
                mobileVer.style.display = 'table';
                desktopVer.style.display = 'none';
                float_table[0].style.margin = '0 0 0 0';
            }
        } 
    }
)
