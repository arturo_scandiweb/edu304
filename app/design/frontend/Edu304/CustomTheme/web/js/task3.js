/**
 * @category    Edu304
 * @package     Edu304/CustomTheme
 * @author      Arturo Arakelyan <arturo@scandiweb.com>
 * @copyright   Copyright (c) 2018 Scandiweb, Inc (https://scandiweb.com)
 * @license     http://opensource.org/licenses/OSL-3.0 The Open Software License 3.0 (OSL-3.0)
 */

require(
    ['jquery'],
    function ($) {
        $('.tocompare').hide();
        $(window).on('resize', function () {
            $('.slick-track > li').css('display', 'block');
        });
    }
)

