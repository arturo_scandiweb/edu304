var config = {
    deps: [
        'js/slick.min',
        'js/task3'
    ],

    map: {
        '*': {
            'slick':'js/slick.min',
            'task3':'js/task3'
        }
    }
};
